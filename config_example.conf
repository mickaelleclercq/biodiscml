# #### BruteForceWeka config file #### #

# IMPORTANT: for classification, do not use classes with numeric attributes

# Working directory. If local, don't set wd.
wd=/home/mickael/ownCloud/Code/BruteForceWeka/

# Debug to show more outputs
debug=true

# Maximum number of cpus to use (enter a value or "max")
# Java is running in low priority by regularly checking cpus available. So
# you can execute other softwares on your server and it will adapt. 
# Just be careful to available memory, limit number of cpus used to avoid out of memory exception.
cpus=max

# Project name, used as prefix for outfiles
project=example

# Temporary folder of 10CV models outputs
cvfolder=tmpCV

# Set infiles here if you have several dataset with a common ID column that will be used for merging
# Only IDs existing in all files will be kept for training, those missing in one of the file will be ignored
# usage: infiles=filename_in_working_directory, identifier
# Ther identifier will be used as a prefix for features of the file, to avoid feature duplicated names. It can be left empty
infile=train_file.expr.csv
infile=train_file.meta.csv
#separator=;

# Merging identifier, used if you have many files to merge
# Column identifier present in all files. 
# Only rows containing identifiers that exist in all files will be considered in the analysis
mergingID=Patient

doClassification=true
classificationKeyword=class

# Set if we perform a regression (numeric class). If yes, set the column class name you want to classify
doRegression=false
regressionKeyword=EDSS

# Max best models
bestModels=1

# CLASSIFICATION algorithms and hyperparameters
# Fast way, pre-determined commands
# usage: cfcmd=classifier with options,optimizer 
# Available optimizers: AUC, ACC, SEN, SPE, MCC, TP+FN, kappa 
classificationFastWay=true
#cfcmd=bayes.NaiveBayes -K, SEN
#cfcmd=bayes.NaiveBayes -K, AUC
#cfcmd=misc.VFI -B 0.4, SEN
#cfcmd=misc.VFI -B 0.4, AUC
cfcmd=bayes.NaiveBayesUpdateable -K, AUC
cfcmd=bayes.NaiveBayes -K, AUC
cfcmd=bayes.NaiveBayes, AUC
cfcmd=bayes.NaiveBayesUpdateable, AUC
#cfcmd=functions.SimpleLogistic -I 0 -P -M 500 -H 50 -W 0.0 -A, AUC
#cfcmd=functions.SimpleLogistic -I 0 -M 500 -H 50 -W 0.0 -A, AUC
#cfcmd=misc.VFI -B 0.4, AUC
cfcmd=bayes.NaiveBayes -D, AUC
#cfcmd=misc.VFI -B 0.6, AUC
#cfcmd=misc.VFI -B 0.3, AUC

# Brute force way
coptimizers=AUC, ACC, SEN, MCC
rCostSensitiveClassifier=false
ccmd=bayes.NaiveBayes -K
ccmd=bayes.NaiveBayes -D
ccmd=bayes.NaiveBayes 
ccmd=misc.VFI -B 0.3
ccmd=misc.VFI -B 0.4
ccmd=misc.VFI -B 0.6
ccmd=trees.J48 -R -N 3 -Q 1 -M 2 -A
ccmd=trees.J48 -S -C 0.25 -M 2
ccmd=trees.J48 -C 0.25 -M 2
ccmd=trees.RandomForest -I 10 -K 0 -S 1
ccmd=trees.RandomForest -I 25 -K 0 -S 1
ccmd=trees.RandomForest -I 50 -K 0 -S 1
ccmd=rules.JRip -F 3 -N 2.0 -O 2 -S 1
ccmd=functions.SPegasos -F 0 -L 1.0E-4 -E 500
ccmd=functions.SPegasos -F 0 -L 1.0E-4 -E 500 -N
ccmd=functions.SPegasos -F 0 -L 1.0E-4 -E 500 -M
ccmd=functions.SPegasos -F 0 -L 1.0E-4 -E 500 -N -M
ccmd=functions.SMO -C 1.0 -L 0.001 -P 1.0E-12 -N 0 -V -1 -W 1 -K "ccmd=functions.supportVector.PolyKernel -C 250007 -E 1.0"
ccmd=functions.SMO -C 1.0 -L 0.001 -P 1.0E-12 -N 1 -V -1 -W 1 -K "ccmd=functions.supportVector.PolyKernel -C 250007 -E 1.0"
ccmd=functions.SMO -C 1.0 -L 0.001 -P 1.0E-12 -N 0 -V -1 -W 1 -K "ccmd=functions.supportVector.NormalizedPolyKernel -C 250007 -E 2.0"
ccmd=functions.SMO -C 1.0 -L 0.001 -P 1.0E-12 -N 1 -V -1 -W 1 -K "ccmd=functions.supportVector.NormalizedPolyKernel -C 250007 -E 2.0"
ccmd=functions.RBFNetwork -B 2 -S 1 -R 1.0E-8 -M -1 -W 0.1
ccmd=functions.RBFNetwork -B 3 -S 1 -R 1.0E-8 -M -1 -W 0.1
ccmd=functions.RBFNetwork -B 4 -S 1 -R 1.0E-8 -M -1 -W 0.1
ccmd=functions.RBFNetwork -B 5 -S 1 -R 1.0E-8 -M -1 -W 0.1
ccmd=functions.RBFNetwork -B 6 -S 1 -R 1.0E-8 -M -1 -W 0.1
ccmd=bayes.AODE -F 1
ccmd=bayes.AODE -F 1 -M -W 1
ccmd=bayes.AODEsr -F 1 -M 1.0 -C 50
ccmd=bayes.AODEsr -F 1 -L -C 50
ccmd=bayes.BayesianLogisticRegression -D -Tl 5.0E-4 -S 0.5 -H 1 -V 0.27 -R R:0.01-316,3.16 -P 1 -F 2 -seed 1 -I 100 -N
ccmd=bayes.BayesianLogisticRegression -D -Tl 5.0E-4 -S 0.5 -H 2 -V 0.27 -R R:0.01-316,3.16 -P 1 -F 2 -seed 1 -I 100 -N
ccmd=bayes.HNB
ccmd=bayes.NaiveBayesSimple
ccmd=bayes.NaiveBayesUpdateable
ccmd=bayes.NaiveBayesUpdateable -K
ccmd=bayes.WAODE
ccmd=bayes.WAODE -I
ccmd=functions.LibLINEAR -S 1 -C 1.0 -E 0.01 -B 1.0
ccmd=functions.LibLINEAR -S 1 -C 1.0 -E 0.01 -B 1.0 -N
ccmd=functions.LibLINEAR -S 1 -C 1.0 -E 0.01 -B 1.0 -N -M
ccmd=functions.LibLINEAR -S 1 -C 1.0 -E 0.01 -B 1.0 -M
ccmd=functions.LibLINEAR -S 1 -C 1.0 -E 0.01 -B 1.0 -Z -N -M
ccmd=functions.LibLINEAR -S 1 -C 1.0 -E 0.01 -B 1.0 -Z -N
ccmd=functions.LibLINEAR -S 1 -C 1.0 -E 0.01 -B 1.0 -Z
ccmd=functions.LibLINEAR -S 1 -C 1.0 -E 0.01 -B 1.0 -Z -N -M -P
ccmd=functions.LibLINEAR -S 1 -C 1.0 -E 0.01 -B 1.0 -N -M -P
ccmd=functions.LibLINEAR -S 1 -C 1.0 -E 0.01 -B 1.0 -M -P
ccmd=functions.LibLINEAR -S 1 -C 1.0 -E 0.01 -B 1.0 -P
ccmd=functions.Logistic -R 1.0E-8 -M -1
ccmd=functions.MultilayerPerceptron -L 0.3 -M 0.2 -N 500 -V 0 -S 0 -E 20 -H a
ccmd=functions.SimpleLogistic -I 0 -M 500 -H 50 -W 0.0
ccmd=functions.SimpleLogistic -I 0 -P -M 500 -H 50 -W 0.0
ccmd=functions.SimpleLogistic -I 0 -P -M 500 -H 50 -W 0.0 -A
ccmd=functions.SimpleLogistic -I 0 -M 500 -H 50 -W 0.0 -A
ccmd=functions.VotedPerceptron -I 1 -E 1.0 -S 1 -M 10000
ccmd=functions.Winnow -I 1 -A 2.0 -B 0.5 -H -1.0 -W 2.0 -S 1
ccmd=functions.Winnow -L -I 1 -A 2.0 -B 0.5 -H -1.0 -W 2.0 -S 1
ccmd=lazy.IB1
ccmd=lazy.IBk -K 1 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
ccmd=lazy.IBk -K 2 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
ccmd=lazy.IBk -K 5 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
ccmd=lazy.IBk -K 1 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
ccmd=lazy.IBk -K 2 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
ccmd=lazy.IBk -K 5 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
ccmd=lazy.IBk -K 1 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
ccmd=lazy.IBk -K 2 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
ccmd=lazy.IBk -K 5 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
ccmd=lazy.IBk -K 1 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
ccmd=lazy.IBk -K 2 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
ccmd=lazy.IBk -K 5 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
ccmd=lazy.IBk -K 1 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
ccmd=lazy.IBk -K 1 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
ccmd=lazy.IBk -K 2 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
ccmd=lazy.IBk -K 5 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
#TODO: IBk: missing BallTree, KDtree and CoverTree
ccmd=lazy.KStar -B 20 -M a
ccmd=lazy.KStar -B 20 -M m
ccmd=lazy.KStar -B 20 -M n
ccmd=lazy.LBR



# REGRESSION algorithms and hyperparameters
# Fast way, pre-determined commands
# Available optimizers: CC, MAE, RMSE, RAE, RRSE 
regressionFastWay=false
rfcmd=functions.GaussianProcesses -L 1.0 -N 1 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0", CC
rfcmd=functions.GaussianProcesses -L 1.0 -N 1 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0", RMSE
#rfcmd=functions.GaussianProcesses -L 1.0 -N 0 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0", CC
#rfcmd=functions.GaussianProcesses -L 1.0 -N 0 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0", RMSE

# Brute force way
roptimizers=CC, RMSE 
rmetaAdditiveRegression=false
rcmd=rules.M5Rules -M 4.0
rcmd=rules.ConjunctiveRule -N 3 -M 2.0 -P -1 -S 1
rcmd=rules.ConjunctiveRule -N 5 -M 2.0 -P -1 -S 1
rcmd=rules.ConjunctiveRule -N 10 -M 2.0 -P -1 -S 1
rcmd=trees.M5P -R -M 4.0
rcmd=trees.M5P -N -R -M 4.0
rcmd=trees.M5P -N -U -R -M 4.0
rcmd=trees.M5P -R -M 2.0
rcmd=trees.M5P -N -R -M 2.0
rcmd=trees.M5P -N -U -R -M 2.0
rcmd=trees.REPTree -M 2 -V 0.001 -N 3 -S 1 -L -1
rcmd=trees.REPTree -M 2 -V 0.001 -N 3 -S 1 -L -1 -P
rcmd=trees.REPTree -M 2 -V 0.001 -N 5 -S 1 -L -1
rcmd=trees.REPTree -M 2 -V 0.001 -N 5 -S 1 -L -1 -P
rcmd=trees.REPTree -M 2 -V 0.001 -N 10 -S 1 -L -1
rcmd=trees.REPTree -M 2 -V 0.001 -N 10 -S 1 -L -1 -P
rcmd=lazy.KStar -B 20 -M a
rcmd=lazy.KStar -B 20 -M m
rcmd=lazy.KStar -B 20 -M n
rcmd=lazy.IBk -K 1 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
rcmd=lazy.IBk -K 1 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
rcmd=lazy.IBk -K 1 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
rcmd=lazy.IBk -K 1 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
rcmd=lazy.IBk -K 1 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
rcmd=lazy.IBk -K 1 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
rcmd=lazy.IBk -K 1 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
rcmd=lazy.IBk -K 1 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ChebyshevDistance -R first-last\""
rcmd=lazy.IBk -K 1 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
rcmd=lazy.IBk -K 2 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
rcmd=lazy.IBk -K 5 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
rcmd=lazy.IBk -K 1 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
rcmd=lazy.IBk -K 2 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
rcmd=lazy.IBk -K 5 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
rcmd=lazy.IBk -K 1 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
rcmd=lazy.IBk -K 2 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
rcmd=lazy.IBk -K 5 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
rcmd=lazy.IBk -K 1 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
rcmd=lazy.IBk -K 2 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
rcmd=lazy.IBk -K 5 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last -V\""
rcmd=lazy.IBk -K 1 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
rcmd=lazy.IBk -K 1 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
rcmd=lazy.IBk -K 1 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
rcmd=lazy.IBk -K 1 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -D -R first-last\""
rcmd=lazy.IBk -K 1 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
rcmd=lazy.IBk -K 1 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
rcmd=lazy.IBk -K 1 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -E -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
rcmd=lazy.IBk -K 1 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
rcmd=lazy.IBk -K 2 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
rcmd=lazy.IBk -K 5 -W 0 -E -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.ManhattanDistance -R first-last\""
#TODO: IBk: missing BallTree, KDtree and CoverTree
rcmd=functions.GaussianProcesses -L 1.0 -N 0 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 1 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 2 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 0 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 2.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 1 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 2.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 0 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 3.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 1 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 3.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 0 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0 -L"
rcmd=functions.GaussianProcesses -L 1.0 -N 1 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0 -L"
rcmd=functions.GaussianProcesses -L 1.0 -N 2 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0 -L"
rcmd=functions.GaussianProcesses -L 1.0 -N 0 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 2.0 -L"
rcmd=functions.GaussianProcesses -L 1.0 -N 1 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 2.0 -L"
rcmd=functions.GaussianProcesses -L 1.0 -N 2 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 2.0 -L"
rcmd=functions.GaussianProcesses -L 1.0 -N 0 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 3.0 -L"
rcmd=functions.GaussianProcesses -L 1.0 -N 1 -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 3.0 -L"
rcmd=functions.GaussianProcesses -L 1.0 -N 0 -K "weka.classifiers.functions.supportVector.NormalizedPolyKernel -C 250007 -E 2.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 1 -K "weka.classifiers.functions.supportVector.NormalizedPolyKernel -C 250007 -E 2.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 2 -K "weka.classifiers.functions.supportVector.NormalizedPolyKernel -C 250007 -E 2.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 0 -K "weka.classifiers.functions.supportVector.NormalizedPolyKernel -C 250007 -E 3.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 1 -K "weka.classifiers.functions.supportVector.NormalizedPolyKernel -C 250007 -E 3.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 2 -K "weka.classifiers.functions.supportVector.NormalizedPolyKernel -C 250007 -E 3.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 0 -K "weka.classifiers.functions.supportVector.NormalizedPolyKernel -C 250007 -E 2.0 -L"
rcmd=functions.GaussianProcesses -L 1.0 -N 1 -K "weka.classifiers.functions.supportVector.NormalizedPolyKernel -C 250007 -E 2.0 -L"
rcmd=functions.GaussianProcesses -L 1.0 -N 2 -K "weka.classifiers.functions.supportVector.NormalizedPolyKernel -C 250007 -E 2.0 -L"
rcmd=functions.GaussianProcesses -L 1.0 -N 0 -K "weka.classifiers.functions.supportVector.NormalizedPolyKernel -C 250007 -E 3.0 -L"
rcmd=functions.GaussianProcesses -L 1.0 -N 1 -K "weka.classifiers.functions.supportVector.NormalizedPolyKernel -C 250007 -E 3.0 -L"
rcmd=functions.GaussianProcesses -L 1.0 -N 2 -K "weka.classifiers.functions.supportVector.NormalizedPolyKernel -C 250007 -E 3.0 -L"
rcmd=functions.GaussianProcesses -L 1.0 -N 0 -K "weka.classifiers.functions.supportVector.Puk -C 250007 -O 1.0 -S 1.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 1 -K "weka.classifiers.functions.supportVector.Puk -C 250007 -O 1.0 -S 1.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 2 -K "weka.classifiers.functions.supportVector.Puk -C 250007 -O 1.0 -S 1.0"
rcmd=functions.GaussianProcesses -L 1.0 -N 0 -K "weka.classifiers.functions.supportVector.RBFKernel -C 250007 -G 0.01"
rcmd=functions.GaussianProcesses -L 1.0 -N 1 -K "weka.classifiers.functions.supportVector.RBFKernel -C 250007 -G 0.01"
rcmd=functions.GaussianProcesses -L 1.0 -N 2 -K "weka.classifiers.functions.supportVector.RBFKernel -C 250007 -G 0.01"
rcmd=functions.LeastMedSq -S 4 -G 0
rcmd=functions.LeastMedSq -S 10 -G 0
rcmd=functions.LibSVM -S 3 -K 0 -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -W 1.0 -seed 1
rcmd=functions.LibSVM -S 3 -K 1 -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -W 1.0 -seed 1
rcmd=functions.LibSVM -S 3 -K 2 -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -W 1.0 -seed 1
rcmd=functions.LibSVM -S 3 -K 3 -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -W 1.0 -seed 1
rcmd=functions.LibSVM -S 4 -K 0 -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -W 1.0 -seed 1
rcmd=functions.LibSVM -S 4 -K 1 -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -W 1.0 -seed 1
rcmd=functions.LibSVM -S 4 -K 2 -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -W 1.0 -seed 1
rcmd=functions.LibSVM -S 4 -K 3 -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -W 1.0 -seed 1
rcmd=functions.LinearRegression -S 0 -C -R 1.0E-8
rcmd=functions.LinearRegression -S 2 -C -R 1.0E-8
rcmd=functions.LinearRegression -S 0 -R 1.0E-8
rcmd=functions.LinearRegression -S 2 -R 1.0E-8
rcmd=functions.RBFNetwork -B 1 -S 1 -R 1.0E-8 -M -1 -W 0.1
rcmd=functions.RBFNetwork -B 2 -S 1 -R 1.0E-8 -M -1 -W 0.1
rcmd=functions.RBFNetwork -B 3 -S 1 -R 1.0E-8 -M -1 -W 0.1
rcmd=functions.RBFNetwork -B 4 -S 1 -R 1.0E-8 -M -1 -W 0.1
rcmd=functions.RBFNetwork -B 5 -S 1 -R 1.0E-8 -M -1 -W 0.1
rcmd=functions.MultilayerPerceptron -L 0.1 -M 0.2 -N 500 -V 0 -S 0 -E 20 -H a
rcmd=functions.MultilayerPerceptron -L 0.3 -M 0.2 -N 500 -V 0 -S 0 -E 20 -H a
rcmd=functions.MultilayerPerceptron -L 0.1 -M 0.1 -N 500 -V 0 -S 0 -E 20 -H a
rcmd=functions.MultilayerPerceptron -L 0.3 -M 0.2 -N 500 -V 0 -S 0 -E 20 -H a
rcmd=functions.MultilayerPerceptron -L 0.1 -M 0.2 -N 1000 -V 0 -S 0 -E 20 -H a
rcmd=functions.MultilayerPerceptron -L 0.3 -M 0.2 -N 1000 -V 0 -S 0 -E 20 -H a
rcmd=functions.MultilayerPerceptron -L 0.1 -M 0.1 -N 1000 -V 0 -S 0 -E 20 -H a
rcmd=functions.MultilayerPerceptron -L 0.3 -M 0.2 -N 1000 -V 0 -S 0 -E 20 -H a
rcmd=functions.SMOreg -C 1.0 -N 0 -I "weka.classifiers.functions.supportVector.RegSMOImproved -L 0.001 -W 1 -P 1.0E-12 -T 0.001 -V" -K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 1.0"
rcmd=functions.SMOreg -C 1.0 -N 0 -I "weka.classifiers.functions.supportVector.RegSMOImproved -L 0.001 -W 1 -P 1.0E-12 -T 0.001 -V" -K "weka.classifiers.functions.supportVector.NormalizedPolyKernel -C 250007 -E 2.0"
rcmd=functions.SMOreg -C 1.0 -N 0 -I "weka.classifiers.functions.supportVector.RegSMOImproved -L 0.001 -W 1 -P 1.0E-12 -T 0.001 -V" -K "weka.classifiers.functions.supportVector.Puk -C 250007 -O 1.0 -S 1.0"
rcmd=functions.SMOreg -C 1.0 -N 0 -I "weka.classifiers.functions.supportVector.RegSMOImproved -L 0.001 -W 1 -P 1.0E-12 -T 0.001 -V" -K "weka.classifiers.functions.supportVector.RBFKernel -C 250007 -G 0.01"
