
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mickael
 */
public class correlation {

    public static void main(String[] args) {
        double[] x = {1, 2, 4, 8};
        //double[] y = {16, 8, 4, 2};
        double[] y = {2,4,8,16};
        double corr = new PearsonsCorrelation().correlation(y, x);
        double spear = new SpearmansCorrelation().correlation(x, y);

        System.out.println(corr);
        System.out.println(spear);
    }

}
